import 'dart:io';

void main(List<String> args) {
  
  print("Masukkan nama : ");
  var nama = stdin.readLineSync().toString();
  print("Masukkan peran : ");
  var peran = stdin.readLineSync().toString();
  if(nama == "" && peran == ""){
    print("Nama harus diisi!");
  }else if(nama == ""){
    print("Nama harus diisi!");
  }else if(peran == ""){
    print("Peran harus diisi!");
  }else if(peran == "werewolf"){
    print("Selamat datang di Dunia Werewolf, " + nama + "!");
    print("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam!");
  }else if(peran == "guard"){
    print("Selamat datang di Dunia Werewolf, " + nama + "!");
    print("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf!");
  }else if(peran == "penyihir"){
    print("Selamat datang di Dunia Werewolf, " + nama + "!");
    print("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
  }else if(peran != "werewolf" || peran != "penyihir" || peran != "guard"){
    print("Harap pilih peran dengan benar! [werewolf, guard atau penyihir]");
  }
}