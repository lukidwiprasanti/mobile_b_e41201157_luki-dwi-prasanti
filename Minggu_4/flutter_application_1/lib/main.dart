import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Drawer"),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            UserAccountsDrawerHeader(
              currentAccountPicture: ClipOval(
                child: Image(
                  image: AssetImage('assets/images/iqbal.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              //membuat nama akun
              accountName: Text("Luki Dwi Prasanti"),
              //membuat nama email
              accountEmail: Text("lukidwi22@gmail.com"),
            ),
            //membuat list menu
            ListTile(
              leading: Icon(Icons.group),
              title: Text("NewGroup"),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.lock),
              title: Text("New Secret Group"),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.notifications),
              title: Text("New Chennel Chat"),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.contacts),
              title: Text("Contacts"),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.bookmark_border),
              title: Text("Saved Message"),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text("Calls"),
              onTap: () {},
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text("Setting"),
              onTap: () {},
            ),
          ],
        ),
      ),
      body: Center(
        child: Text(
          "Click Here",
          style: TextStyle(fontSize: 35),
        ),
      ),
    );
  }
}
